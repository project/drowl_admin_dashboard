# DROWL Admin Dashboard

## Contents of this file

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Introduction

Provides a better and central administrative overview for users with
"Access Admin Dashboard" permission, using the power of

- page_manager
- layout_builder

Get a nice default, configurable Dashboard for your Drupal project and provide
a better onboarding experience for your maintainers.


## Requirements

This module requires the following modules:

- [DROWL Admin](https://www.drupal.org/project/drowl_admin)
- [menu Block](https://www.drupal.org/project/menu_block)
- [Page Manager](https://www.drupal.org/project/page_manager)
- [User Permission Condition](https://www.drupal.org/project/user_permission_condition)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will simply provide a better and central administrative
overview for users.


## Maintainers

- Julian Pustkuchen (Anybody) - https://www.drupal.org/u/anybody
- Thomas Frobieter (thomas.frobieter) - https://www.drupal.org/u/thomasfrobieter
- Joshua Sedler (Grevil) - https://www.drupal.org/u/grevil

Development proudly sponsored by German Drupal Friends & Companies:

- [webks: websolutions kept simple](https://www.webks.de)
- [DROWL: Drupalbasierte Lösungen aus Ostwestfalen-Lippe (OWL), Germany](https://www.drowl.de)
